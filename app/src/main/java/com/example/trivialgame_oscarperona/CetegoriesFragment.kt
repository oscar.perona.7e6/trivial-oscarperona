package com.example.trivialgame_oscarperona

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.trivialgame_oscarperona.databinding.FragmentCetegoriesBinding
import com.example.trivialgame_oscarperona.databinding.FragmentMenuBinding


class CetegoriesFragment : Fragment() {

    lateinit var binding: FragmentCetegoriesBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentCetegoriesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.prueba.setOnClickListener{
            val categori = "prueba completada"
            val action = CetegoriesFragmentDirections.actionCetegoriesFragmentToQuestionFragment(categori)
            findNavController().navigate(action)
        }
    }


}