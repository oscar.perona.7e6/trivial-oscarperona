package com.example.trivialgame_oscarperona

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController

import com.example.trivialgame_oscarperona.databinding.FragmentMenuBinding

class MenuFragment : Fragment() {

    lateinit var binding: FragmentMenuBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMenuBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.playButton.setOnClickListener{
           /* parentFragmentManager.beginTransaction().apply {
                replace(R.id.fragmentContainerView, CetegoriesFragment())
                setReorderingAllowed(true)
                addToBackStack("name")
                commit()
            }*/
            //findNavController().navigate(R.id.action_menu_to_game)
            findNavController().navigate(R.id.action_menuFragment_to_cetegoriesFragment)
        }
    }

}