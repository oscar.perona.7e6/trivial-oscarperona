package com.example.trivialgame_oscarperona

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.trivialgame_oscarperona.databinding.ActivityMainBinding
import com.example.trivialgame_oscarperona.databinding.FragmentMenuBinding

lateinit var binding: ActivityMainBinding

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

}